import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {OrdersService} from "../orders.service";
import {
  deleteOrderAPISuccess,
  invokeDeleteOrderAPI,
  invokeSaveOrderAPI,
  invokeUpdateOrderAPI,
  saveOrderAPISuccess, updateOrderAPISuccess
} from "./order.action";
import {map, switchMap} from "rxjs";
import {Appstate} from "../../shared/store/appstate";
import {Store} from "@ngrx/store";
import {setAPIStatus} from "../../shared/store/app.action";
import {ApiService} from "../api.service";


@Injectable()
export class OrderEffects {
  constructor(private actions$: Actions,
              private orderService: OrdersService,
              private store: Store, private apiService: ApiService,
              private appStore: Store<Appstate>
  ) {
  }

  /* Reading the input method and
  create the data in the local storage */
  saveNewOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(invokeSaveOrderAPI),
      switchMap((action) => {
        return this.orderService
          .create(action.payload)
          .pipe(map((data) => {
            this.appStore.dispatch(setAPIStatus({apiStatus: {apiResponseMessage: '', apiStatus: 'Success'}}))
            return saveOrderAPISuccess({response: data})
          }))
      })
    )
  )

  /* Reading the input method and
    update the data in the local storage */
  updateOrder$ = createEffect(() =>
    this.actions$.pipe(
      //calling api
      ofType(invokeUpdateOrderAPI),
      switchMap((action) => {
        return this.orderService
          .update(action.payload)
          .pipe(map((data) => {
            this.appStore.dispatch(setAPIStatus({apiStatus: {apiResponseMessage: '', apiStatus: 'Success'}}))
            return updateOrderAPISuccess({response: data})
          }))

      })
    )
  )

  /* Reading the input method and
    Delete the data in the local storage */
  deleteOrder$ = createEffect(() =>
    this.actions$.pipe(
      //calling api
      ofType(invokeDeleteOrderAPI),
      switchMap((action) => {
        return this.orderService
          .delete(action.id)
          .pipe(map((data) => {
            this.appStore.dispatch(setAPIStatus({apiStatus: {apiResponseMessage: '', apiStatus: 'Success'}}))
            return deleteOrderAPISuccess({id: action.id})
          }))
      })
    )
  )
}
