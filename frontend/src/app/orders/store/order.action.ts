import {createAction, props} from "@ngrx/store";
import {Order} from "./order";

/* invokeSaveOrderAPI - we are sending the action for
   creating the order */
export const invokeSaveOrderAPI = createAction(
  "[Order API] invoke save order API",
  props<{ payload: Order }>()
)

/* saveOrderAPISuccess - we are sending the action for
   status after creating the order */
export const saveOrderAPISuccess = createAction(
  "[Order API] save order API success",
  props<{ response: Order }>()
)

/* invokeUpdateOrderAPI - we are sending the action for
   update the order */
export const invokeUpdateOrderAPI = createAction(
  "[Order API] invoke update order API",
  props<{ payload: Order }>(),
)

/* updateOrderAPISuccess - we are sending the action for
   status after update the order */
export const updateOrderAPISuccess = createAction(
  "[Order API] update order API success",
  props<{ response: Order }>()
)

/* invokeDeleteOrderAPI - we are sending the action for
   delete the order */
export const invokeDeleteOrderAPI = createAction(
  "[Order API] invoke delete order API",
  props<{ id: number }>()
)

/* deleteOrderAPISuccess - we are sending the action for
   status after delete the order */
export const deleteOrderAPISuccess = createAction(
  "[Order API] delete order API success",
  props<{ id: number }>()
)
