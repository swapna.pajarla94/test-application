import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderComponent } from './order.component';
import { FormBuilder } from '@angular/forms';
import { OrdersService } from "../orders.service";
import { Store } from "@ngrx/store";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastService } from "../toast.service";
import { ApiService } from "../api.service";
import {Appstate} from "../../shared/store/appstate";

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let fakeOrderService: jasmine.SpyObj<OrdersService>;
  let fakeModel: jasmine.SpyObj<NgbModal>;
  let fakeAppStore: jasmine.SpyObj<Store<Appstate>>;
  let fakeToastService: jasmine.SpyObj<ToastService>;
  let fakeFb: jasmine.SpyObj<FormBuilder>;
  let fakeStore: jasmine.SpyObj<Store>;
  let fakeApi_sevice: jasmine.SpyObj<ApiService>;

  beforeEach(waitForAsync(() => {
    fakeOrderService = jasmine.createSpyObj<OrdersService>('OrdersService', ['getOrderData']);
    fakeModel = jasmine.createSpyObj<NgbModal>('NgbModal', []);
    fakeAppStore = jasmine.createSpyObj<Store<Appstate>>('Store<Appstate>', ['pipe', 'dispatch']);
    fakeToastService = jasmine.createSpyObj<ToastService>('ToastService', ['successMessage', 'errorMessage']);
    fakeFb = jasmine.createSpyObj<FormBuilder>('FormBuilder', ['group']);
    fakeStore = jasmine.createSpyObj<Store>('Store', ['pipe', 'dispatch']);
    fakeApi_sevice = jasmine.createSpyObj<ApiService>('ApiService', ['update', 'getOrder']);

    TestBed.configureTestingModule({
      declarations: [OrderComponent],
      providers: [
        { provide: OrdersService, useFactory: () => fakeOrderService },
        { provide: NgbModal, useFactory: () => fakeModel },
        { provide: Store, useFactory: () => fakeAppStore },
        { provide: ToastService, useFactory: () => fakeToastService },
        { provide: FormBuilder, useFactory: () => fakeFb },
        { provide: Store, useFactory: () => fakeStore },
        { provide: ApiService, useFactory: () => fakeApi_sevice },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
