import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {
  PageChangeEvent,
  AddEvent,
  EditEvent,
  GridComponent,
  SaveEvent,
  RemoveEvent,
  CancelEvent
} from "@progress/kendo-angular-grid";
import {SortDescriptor} from "@progress/kendo-data-query";
import {faEdit, faClose} from "@fortawesome/free-solid-svg-icons";
import {OrdersService} from "../orders.service";
import {select, Store} from "@ngrx/store";
import {Order} from "../store/order";
import {invokeDeleteOrderAPI, invokeSaveOrderAPI, invokeUpdateOrderAPI} from "../store/order.action";
import {selectOrders} from "../store/order.selector";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {selectAppState} from "../../shared/store/app.selector";
import {setAPIStatus} from "../../shared/store/app.action";
import {Appstate} from "../../shared/store/appstate";
import {ToastService} from "../toast.service";
import {ApiService} from "../api.service";
import {orderErrorMessages} from "../error";


declare var window: any;

@Component({
  selector: 'src-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})

export class OrderComponent implements OnInit {
  title = 'GreenIT Application Challenge';
  public faEdit = faEdit;
  public updated_data: any;
  public faClose = faClose;
  csvData: any = [];
  public gridView: any = {};
  public templates: any = [];
  public pageSize = 10;
  public pageNumber = 1;
  public skip = 0;
  public take = 10;
  public allowUnsort = true;
  public sort: SortDescriptor[] = [];
  public pageSizes = [10, 25, 50, 100];
  submitted = false;
  private editedRowIndex: number;
  public gridData: any = {
    data: []

  }
  public gridSelectRow: any = [];
  idTodelete: number = 0;
  deleteModal: any;

  public disabled = true;
  public editFromGroup: any;
  public setForm: any;
  public createFromGroup: any;
  public edit_id: any;
  public lo_so_success: any;
  public is_edit: boolean = true;
  public is_create: boolean = true;
  public is_delete: boolean = true;

  order$ = this.store.pipe(select(selectOrders));

  constructor(private orderService: OrdersService, private model: NgbModal, private appStore: Store<Appstate>,
              private toastService: ToastService, private fb: FormBuilder, private store: Store, private api_sevice: ApiService
  ) {
  }

  ngOnInit(): void {
    /* Get the Order Details while loading page */
    this.getOrderData()

    //Conformation for Delete Modal
    this.deleteModal = new window.bootstrap.Modal(
      document.getElementById("deleteModal")
    );

  };

  /* Pagination of the Grid */
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.take = event.take;
    this.pageNumber = Math.floor(this.skip / this.pageSize) + 1;
    this.pageSize = this.take;

  }

  /** Sorting the Grid **/
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
  }

  /* Column settings - Kendo Grid */
  public fileColumns = [
    {
      title: "Id",
      field: "id",
    },
    {
      title: "Name",
      field: "name",
      editor: true,
    },
    {
      title: "State",
      field: "state",
      editor: true,
    },
    {
      title: "Zip",
      field: "zip",
      editor: true,
    },
    {
      title: "Amount",
      field: "amount",
      editor: true,
    },
    {
      title: "Quantity",
      field: "quantity",
      editor: true,
    },
    {
      title: "Item",
      field: "item",
      editor: true,
    },
  ];

  /* Close the editor for the given row */
  public cancelHandler(args: CancelEvent): void {
    this.closeEditor(args.sender, args.rowIndex);
    this.retriveButtons();
  }

  // Define all editable fields validators and default values
  // Put the row in edit mode
  public editOrderData(args: EditEvent): void {
    this.is_edit = this.is_create = this.is_delete = false;
    const {dataItem} = args;
    this.edit_id = dataItem.id
    this.closeEditor(args.sender);
    this.editFromGroup = new FormGroup({
      name: new FormControl(dataItem.name, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]),
      state: new FormControl(dataItem.state, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]),
      zip: new FormControl(dataItem.zip, [Validators.required, Validators.pattern("^[0-9]{6}$")]),
      amount: new FormControl(dataItem.amount, [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]),
      quantity: new FormControl(dataItem.quantity, [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]),
      item: new FormControl(dataItem.item, [Validators.required, Validators.pattern("^[a-zA-Z0-9]{3,10}$")]),
    });
    this.editedRowIndex = args.rowIndex;
    args.sender.editRow(args.rowIndex, this.editFromGroup);
  }

  // Close the editor in the grid
  private closeEditor(grid: GridComponent, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
  }

  public retriveButtons(){
    this.deleteModal.hide();
    this.is_edit = this.is_create = this.is_delete =  true
  }

  // While saving or creating the data in Grid
  public saveOrderData({sender, rowIndex, formGroup, isNew}: SaveEvent): any {

    //Check if the all the form controls is valid or not
    let check_the_formGroup_valid = this.checkFormControls(formGroup);

    if (check_the_formGroup_valid.length == 0) {

      if (!isNew) {
        // For Updating the row data in local and server
        this.setForm = this.editFromGroup.value;
        this.setForm.id = this.edit_id;
        // For Updating the row data in local
        this.store.dispatch(invokeUpdateOrderAPI({payload: {...this.setForm}}));
        if (this.lo_so_success !== 'Success') {
          let appStatus$ = this.appStore.pipe(select(selectAppState))
          appStatus$.subscribe((data) => {
            if (data.apiStatus === 'Success') {
              this.lo_so_success = 'Success';
              // For Updating the row data in server
              this.updateData();
              this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
              this.toastService.successMessage('Order Updated successfully!', 'Order');
            }
          })
        }
      }

      // For create the new Row data in local and server
      else {
        this.store.dispatch(invokeSaveOrderAPI({payload: {...this.createFromGroup.value}}));
        if (this.lo_so_success !== 'Success') {
          let appStatus$ = this.appStore.pipe(select(selectAppState))
          appStatus$.subscribe((data) => {
            if (data.apiStatus === 'Success') {
              this.lo_so_success = 'Success';
              // For creating the new row data in server
              this.updateData();
              this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
              this.toastService.successMessage('Order Created successfully!', 'Order');
            }
          })
        }
      }
      sender.closeRow(rowIndex);
    }else {

      this.toastService.errorMessage(check_the_formGroup_valid[0], 'Order');
    }
  }

  /* Open a new item editor in the grid to add the new record */
  public createOrder(args: AddEvent): void {
    this.is_edit = this.is_delete = false;
    this.closeEditor(args.sender);
    // define all editable fields validators and default values
    this.createFromGroup = this.fb.group({
      name: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      state: ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      zip: ['', [Validators.required, Validators.pattern("^[0-9]{6}$")]],
      amount: ['', [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]],
      quantity: ['', [Validators.required, Validators.pattern("^[1-9]\\d*(\\.\\d+)?$")]],
      item: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{3,10}$")]]
    });
    args.sender.addRow(this.createFromGroup);
  }

  // Pop up Modal to Delete
  openDeleteModal(args: RemoveEvent) {
    this.idTodelete = args.dataItem.id;
    this.deleteModal.show();
  }

  // To Delete the order in the local storage and server
  confirmDelete() {
    this.store.dispatch(invokeDeleteOrderAPI({id: this.idTodelete}));
    this.deleteModal.hide();
    if (this.lo_so_success !== 'Success') {
      // // To Delete the order in the local
      let appStatus$ = this.appStore.pipe(select(selectAppState))
      appStatus$.subscribe((data) => {
        if (data.apiStatus === 'Success') {
          this.lo_so_success = 'Success';
          // To Delete the order in server
          this.updateData();
          this.appStore.dispatch(setAPIStatus({apiStatus: {apiStatus: '', apiResponseMessage: ''}}));
          this.toastService.successMessage('Order Deleted successfully!', 'Order');
        }
      })
    }
  }

  // To create,update and delete the order in the server
  updateData() {
    //Get the details from the db-json
    this.orderService.getOrderData().subscribe((response: Order) => {
      //Sending the Local updated to Server
      if (response) {
        this.api_sevice.update(response).subscribe((result: any) => {
          if (result['success']) {
            this.retriveButtons();
            this.getOrderData();
          } else {
            this.toastService.errorMessage("Order Failed!!", 'Order');
          }
        })
      }
    });
  }

  // To get the order details from the server
  getOrderData(): any {
    this.api_sevice.getOrder().subscribe((response: Order) => {
      if (response) {
        this.updated_data = response;
        this.gridData = {
          data: response
        };
        this.gridView = response;
      } else {
        this.toastService.errorMessage("Fetch Failed!!", 'Order');
      }
    })
  }

  /* Before sending it to the server
     we are validating our code here
    -- if formcontrol is invalid in frontend
     only it will throw error no need to send server and validate --
   */

  checkFormControls(fromGroup: any) {
    const invalid = [];
    let message = '';
    const controls = fromGroup.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        if (name == 'name') {
          message = orderErrorMessages.namePattern;
          invalid.push(message);
        } else if (name == 'state') {
          message = orderErrorMessages.statePattern;
          invalid.push(message);
        } else if (name == 'zip') {
          message = orderErrorMessages.zipPattern;
          invalid.push(message);
        } else if (name == 'amount') {
          message = orderErrorMessages.amountPattern;
          invalid.push(message);
        } else if (name == 'quantity') {
          message = orderErrorMessages.quantityPattern;
          invalid.push(message);
        } else if (name == 'item') {
          message = orderErrorMessages.itemPattern;
          invalid.push(message);
        }
      }
    }
    return invalid
  }

}
