import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastr: ToastrService) {
  }

  // Displaying the success messages
  successMessage(message: string | undefined, title: string | undefined) {
    this.toastr.success(message, title)
  }

  // Displaying the error messages
  errorMessage(message: string | undefined, title: string | undefined) {
    this.toastr.error(message, title)
  }
}
