import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Order} from "./store/order";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  constructor(private http:HttpClient) { }

  //Get the order data in db-json(local Storage)
  getOrderData(): Observable<Order> {
    return this.http.get<Order>(`http://localhost:3000/orders`)
  }

  //Create the order data in db-json(local Storage)
  create(payload: Order){
    return this.http.post<Order>(`http://localhost:3000/orders`, payload)
  }

  //Update the order data in db-json(local Storage)
  update(payload: Order){
    return this.http.put<Order>(`http://localhost:3000/orders/${payload.id}` , payload)
  }

  //Delete the order data in db-json(local Storage)
  delete(id:number){
    return this.http.delete(`http://localhost:3000/orders/${id}`)
  }

}
