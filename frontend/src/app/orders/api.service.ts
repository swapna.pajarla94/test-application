import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Order} from "./store/order";
import {Observable} from "rxjs";
import {OrdersService} from "./orders.service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private api_server = "http://localhost/backend";

  /* Http to connect server
   * Content type as JSON
   */
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) {}

  /* To get the order details from the server */
  getOrder(): Observable<Order> {
    return this.httpClient.get<Order>(this.api_server + '/getOrderdata')
  }

  /* To update the order details in the server */
  update(updated_data: any) {
    return this.httpClient.post<Order>(this.api_server + '/update', JSON.stringify(updated_data), this.httpOptions);
  }

}
