import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[inputSelector]'
})
export class InputrestrictionDirective {

  @Input('inputrestriction') SampleDAta : string;

  private element : ElementRef;

  constructor(element : ElementRef) {
    this.element = element;
  }

  @HostListener('keypress', ['$event'])
  handleKeyPress(event : KeyboardEvent)
  {

    var regex = new RegExp(this.SampleDAta);
     console.log(regex);
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    console.log(str);
    if (regex.test(str)) {
      console.log('true');
      return true;
    }

    event.preventDefault();
    return false;
  }


}

