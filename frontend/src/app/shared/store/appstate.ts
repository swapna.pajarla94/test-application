// Set the API status response
export interface Appstate {
  apiStatus : string,
  apiResponseMessage: string
}
