import {createFeatureSelector} from "@ngrx/store";
import {Appstate} from "./appstate";

// To select the state of the order
export const selectAppState = createFeatureSelector<Appstate>('myappstate');
