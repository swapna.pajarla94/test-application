import {createReducer, on} from "@ngrx/store";
import {Appstate} from "./appstate";
import {setAPIStatus} from "./app.action";

export const initialState: Appstate ={
  apiStatus : '',
  apiResponseMessage: ''
}

// To Retrive the api status after updating order details in the local storage
export const appReducer= createReducer(
  initialState,
  on(setAPIStatus,(state,{apiStatus}) =>{
    return apiStatus
  })
)
