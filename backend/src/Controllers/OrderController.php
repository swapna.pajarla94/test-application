<?php

namespace App\Controllers;

require __DIR__ . "/../Interface/OrderInterface.php";
require __DIR__ . "/../Traits/ResponseMessageTrait.php";

use Exception;
use interfaces\OrderInterface;
use App\Traits\ResponseMessageTrait;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class OrderController implements OrderInterface
{
    use ResponseMessageTrait;

    private $csvFilePath = '';
    private $logger;

    public function __construct(string $csvFilePath)
    {
        $this->csvFilePath = $csvFilePath;
        /**
         * Create object of Logger with channel-name Info
         */
        $this->logger = new Logger("info");
        $stream_handler = new StreamHandler(__DIR__ . "\..\logs\error_log.log", Logger::DEBUG);
        $this->logger->pushHandler($stream_handler);
    }

    /** requestControl this method always
     * calls first in the controller it will
     * send the methods for the functions
     * which method have to be called
     */
    public function requestControl(string $method, ?string $action)
    {
        //To get the records from the Csv file
        if ($method == 'GET') {
            return $this->getOrderData($this->csvFilePath);
        } //To Create/Update/Delete in the CSV file
        else if($method == 'POST'){
            $data = (array)json_decode(file_get_contents("php://input"), true);
            return $this->saveOrderData($data);
        }
    }

    // To Fetch the orders details from the CSV
    public function getOrderData(string $csvFilePath)
    {
        try {
            //Check if the file is exists or not
            $file_exists_or_not = $this->checkCsvFileExistsOrNot($this->csvFilePath);
            if($file_exists_or_not) {
                $orderArray = [];
                $headers = [];
                $count = 0;
                //Opening the file in Read Mode
                if (($fhandle = fopen($this->csvFilePath, "r")) !== FALSE) {
                    while (($data = fgetcsv($fhandle, 1000, ",")) !== FALSE) {
                        //Pushing the first row in the headers
                        if ($count == 0) {
                            $headers[] = $data;
                        } else {
                            //pushing the $orderArray Data
                            $orderArray[] = $data;
                        }
                        $count++;
                    }
                    //Closing the CSV file
                    fclose($fhandle);
                    //Structuring the headers with data in the csv
                    for ($i = 0; $i <= (count($orderArray) - 1); $i++) {
                        $listRecords[] = array_combine($headers[0], $orderArray[$i]);
                    }
                }
                echo json_encode($listRecords, true);
            }else{
                throw new Exception("CSV File does not exists");
            }
        } catch (Exception $e) {
            $response = $e->getMessage();
            $this->logger->error($response);
            return $this->errorResponse($response, false, 304);
        }
    }

    /* For Create, update and delete this metjod will call */
    public function saveOrderData(array $data)
    {
        try {
            $file_exists_or_not = $this->checkCsvFileExistsOrNot($this->csvFilePath);
            if ($file_exists_or_not) {
                //Open CSV file in write mode
                if (($fhandle = fopen($this->csvFilePath, "w")) !== FALSE) {
                    $header = array_keys($data[0]);
                    fputcsv($fhandle, $header);
                    foreach ($data as $rows) {
                        fputcsv($fhandle, $rows);
                    }
                    $result = fclose($fhandle);
                    if ($result) {
                        return $this->successResponse(true, true, 200);
                    } else {
                        throw new Exception("Order Operation Create/Edit/Delete Failed");
                    }
                }
            } else {
                throw new Exception("CSV File does not exists");
            }
        } catch (Exception $e) {
            $response = $e->getMessage();
            $this->logger->error($response);
            return $this->errorResponse($response, false, 304);
        }
    }

    // Check if the File exists or Not
    public function checkCsvFileExistsOrNot($csvFilePath)
    {
        try {
            //Check if file exists or not
            if (!file_exists($csvFilePath)) {
                throw new Exception("File does not exists");
            }
            return true;
        } catch (Exception $e) {
            $response = $e->getMessage();
            $this->logger->error($response);
            return $this->errorResponse($response, false, 304);
        }
    }
}







