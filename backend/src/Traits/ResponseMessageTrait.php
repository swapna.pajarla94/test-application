<?php

namespace App\Traits;

trait ResponseMessageTrait
{

    // It will call for sending response as success
    public function successResponse($data, $statue = true, $code)
    {
        header("Content-type: application/json; charset=UTF-8");
        echo json_encode(['success' => $statue, 'data' => $data], $code);
    }

    // It will call for sending response as Failed
    public function errorResponse($message, $statue, $code)
    {
        header("Content-type: application/json; charset=UTF-8");
        echo json_encode(['success' => $statue, 'error' => $message, 'code' => $code], $code);
    }

}