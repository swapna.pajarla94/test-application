<?php

namespace Interfaces;

interface OrderInterface
{
    /**
     * requestControl based on GET, POST and actions
     * this function acts as a route helps in directing to 
     * controller function based on actions (getdata, create ,update , delete etc.)
     */
    public function requestControl(string $method, ?string $action);
    /**
     * getOrderData
     * reads all the rows of (order) csv file
     * then returns the array
     */
    public function getOrderData(string $csvFilePath);
    /**
     * updateOrderData
     * read order data from (order) csv file, compare row-id with given id and
     * Update the particular array
     */
    public function saveOrderData(array $data);

}
