<?php
// enable type restriction
declare(strict_types=1);

use App\Controllers\OrderController;

require __DIR__ . "/vendor/autoload.php";
// load packages installed from packages

// define root path of application
const APPROOT = __DIR__ . '/src/';

/* Set header configurations */
header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type');
/* This will allow any request comes from any origin */
header('Access-Control-Allow-Origin: *');
header('application/x-www-form-urlencoded');
/* Collect input data from browser request and which type of content*/
header('Content-Type: application/json, charset=utf-8');
/*It will allow any GET, POST, or OPTIONS requests from any origin. */
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

// instantiate SPL (Standard PHP Library) Autoloader
spl_autoload_register(function ($class) {
    require APPROOT . "Controllers/$class.php";
});

// Getting methods from frontend
$methods = explode("/", $_SERVER["REQUEST_URI"]);


$controller = new OrderController(__DIR__ . '\src\data.csv');
$id = $parts[4] ?? null;
$controller->requestControl($_SERVER["REQUEST_METHOD"], $methods[2]);











