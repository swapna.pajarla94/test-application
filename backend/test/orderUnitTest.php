<?php

namespace Tests;

use App\Controllers\OrderController;
use PHPUnit\Framework\TestCase;

require __DIR__ . "/../src/Controllers/OrderController.php";

class orderUnitTest extends TestCase
{
    private $csv_file_path = 'test/test_data.csv';

    /* Checking file is exist or not */
    public function testFileExist()
    {
        // Assert function to test whether given
        // file path exists or not
        $this->assertFileExists(
            $this->csv_file_path,
            "File doesn't exists"
        );
    }

    /* Checking the file is Readble or Not */
    public function testFileIsReadble()
    {
        $this->assertFileIsReadable(
            $this->csv_file_path,
            "File is not Readble"
        );
    }

   /* Checking the file is Writable or Not */
    public function testFileIsWritable()
    {
        $this->assertFileIsWritable(
            $this->csv_file_path,
            "File is not Writable"
        );
    }

    /* Checking to get the order data or not */
    public function testReadData()
    {
        $dataControllerbject = new OrderController($this->csv_file_path);

        // Get CSV order data
        $result = $dataControllerbject->getOrderData($this->csv_file_path);
        $this->assertIsArray($result, "returns array on success");

        // Comparing with length of the csv data
        $this->assertGreaterThanOrEqual(2, count($result));
    }

    //Test Function for Delete
    public function testDeleteTest()
    {
        $id=1;
        $dataControllerbject = new OrderController($this->csv_file_path);
        $result = $dataControllerbject->saveOrderData($id);
        $this->assertJson($result);
        $this->assertNotContains($result, $dataControllerbject->getOrderData($this->csv_file_path),"There was problem removing your data");
        $this->assertTrue((bool)$result);
    }

}